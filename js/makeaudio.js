function minAndSec(sec) {
   	sec = parseInt(sec);
    return  Math.floor(sec / 60) + ":" + (sec % 60 < 10 ? '0' : '') + Math.floor(sec % 60);
	
}
function setVolume(myVolume) {
    var myMedia = document.getElementById('audioPlayer');
    myMedia.volume = myVolume;
}



 		
 
     
var makeAudio = function(){
    var durration       = $("#media .duration").text();
    var secondsTotal    = durration;
    var $audio          = $('audio'), audioEl = $audio[0];
       
    var audio = {
        currentTime: 0,
        duration: secondsTotal,
		
        volume: 0.5,
        set: function(key, value) {
            this[key] = value;
            try { audioEl[key] = value; } catch(e) {}
            if (key == 'currentTime') {
                $audio.trigger('timeupdate');
            }
            if (key == 'volume') {
                $audio.trigger('volumechange');
            }
        },
        play: function() {
            audioEl.play && audioEl.play();
            resetScroll();
             
        },
        pause: function() {
            audioEl.pause && audioEl.pause();
        }
    };
    
	
	
	
    $audio.bind('timeupdate', function() {
		audio.currentTime = audioEl.currentTime;
    });
    audio.set('currentTime', 0);
    audio.set('volume', 0.5);
     
     
    $(document).keydown(function(e) {
          if (e.keyCode == '32') {
            $('.mplayer .playpause').trigger('click');
          }
    });
     
    // Create the play/pause button using two toggling jQuery UI icons
    $('.mplayer .playpause').click(function() {
        var player  = $(this).parents('.mplayer');
        var started = "You have listened to <span class='percent bold'></span><b>%</b> of this piece.";
         
         
        if ($('.track').hasClass('notStarted')){
            $('.track').removeClass('notStarted');
            $('.mplayer .time-container .completion').html(started);
        }
		
         
        if (player.is('.paused')) {
            $('.mplayer').removeClass('paused');
            $('.mplayer .playpause .pause').css({'display': 'block'})
            audio.play();
             
        } else {
            $('.mplayer').addClass('paused');
            $('.mplayer .playpause .pause').css({'display': 'none'})
            audio.pause();
        }
        return false;
         
    });
     
     
         
    $('.mplayer').addClass('paused');
     
    // prevent text selection of text in time labels
    $('.mplayer .currenttime, .mplayer .duration').disableSelection();
 
 
     
    $("#row1 a").click(function(i){
        var musicStart  = $(this).attr('data-begin');
        var musicCurrent = $("#row1 a.active").attr('data-begin');
        barMoved = true; 
        $("#row1 a, #row2 a").removeClass('active');
        $(this).addClass('active');
        $('#row2 a[data-begin="'+musicStart+'"]').addClass('active');
         
        if ($('.mplayer').hasClass('paused')){
            audio.pause();
            audio.set('currentTime', musicStart);
             
        } else{
            audio.pause();
            audio.set('currentTime', musicStart);
            audio.play();
             
        }
        findAnnotation();
        resetScroll(); 
         
         
    });
         
    $("#row2 a").click(function(i){
         
        var musicStart = $(this).attr('data-begin');
        var musicCurrent = $("#row2 a.active").attr('data-begin');
        barMoved = true; 
        $("#row2 a").removeClass('active');
        $(this).addClass('active');
         
        if ($('.mplayer').hasClass('paused')){
            audio.pause();
            audio.set('currentTime', musicStart);
             
        } else{
            audio.pause();
            audio.set('currentTime', musicStart);
            audio.play();
             
        }
         
        findAnnotation();
        resetScroll(); 
         
    });
     
    var playPause = 0;
 
// create a min range slider on the track for the current position in the song
    $('.mplayer .track').slider({
        range: 'min',
        max: secondsTotal,
        slide: function(event, ui) {
                $('.ui-slider-handle', this).css('margin-left', (ui.value < 3) ? (1 - ui.value) + 'px' : '');

                 barMoved = true;
                
		if (ui.value >= 0 && ui.value <= audio.duration) {
                    audio.set('currentTime', ui.value);
                }
             
        },
        change: function(event, ui) {
            $('.ui-slider-handle', this).css('margin-left', (ui.value < 3) ? (1 - ui.value) + 'px' : '');
             
        },
        start: function(event, ui) {
            if ($('.mplayer').hasClass('paused')){
                playPause = 0;
            } else {
                playPause = 1;
            }
            return playPause;
            audio.pause();
        },
        stop: function(event, ui) {
            //console.log(playPause)
            if (playPause == 0){
                audio.pause();
            }  else{
                audio.play();
            }
            
        }
    })
    .find('.ui-slider-handle').css('margin-left', '0').end()
    .find('.ui-slider-range').addClass('ui-corner-left').end();
     
     
    // create a semi-transparent progressbar on the track for displaying (simulated) caching of music file
    var secondsCached = 0, cacheInterval;
    $('.mplayer .track').progressbar({
        value: secondsCached / secondsTotal * 100
    })
    .find('.ui-progressbar-value').css('opacity', 0.2).end();
             
    // simulate media file caching
    cacheInterval = setInterval(function() {
        secondsCached += 2;
        if (secondsCached > secondsTotal) clearInterval(cacheInterval);
        $('.mplayer .track.ui-progressbar').progressbar('value', secondsCached / secondsTotal * 100);
		
    }, 30);
     
     
    /* VOLUME CONTROL */   
    $("#volume").slider({
        min: 0,
        max: 100,
        value: 50,
        range: "min",
        animate: true,
        slide: function(event, ui) {
            setVolume((ui.value) / 100);
        }
    });
    var volumeinc  = 10;
     
    $('.volCtrl.max').click(function(){
        var currentVol = $('#volume').slider('value');
        if (currentVol >= '100'){
            return false;      
        } else {
            currentVol +=volumeinc;
            newVolume = (currentVol/100);
            //console.log(newVolume)
            $('#volume').slider('value', currentVol);
             
            audio.set('volume', newVolume);
        }
         
    });
    $('.volCtrl.min').click(function(){
        var currentVol = $('#volume').slider('value');
        if (currentVol <= '0'){
            return false;
             
        } else{
            currentVol -=volumeinc;
            newVolume = (currentVol/100);
            $('#volume').slider('value', currentVol);
            audio.set('volume', newVolume);
        }
         
    });
    /* END VOLUME CONTROL */
    
	
    $('.mplayer .currenttime').text(minAndSec(audio.currentTime.toFixed()));
    $('.mplayer .duration').text(minAndSec(secondsTotal));
 
    // bind to audio events to keep buttons, track, and volume synced
    $audio.bind('timeupdate', function(event) {
        $('.mplayer .track').each(function() {
            if ($(this).slider('value') != audio.currentTime) {
                $(this).slider('value', audio.currentTime);
                 
            }
        });
         
        $('.mplayer .currenttime').text(minAndSec(audio.currentTime.toFixed()));
         
         
        findSection();
        percentage();
        
    });

	$audio.bind("ended", function(){ 
		audio.pause();
        $('.mplayer').addClass('paused');
        $('.mplayer .playpause .pause').css({'display': 'none'});
		audio.set('currentTime', 0);
	}); 
	
	    
    
     
     
    var findSection = function(){
        var currentTime = $(audio.currentTime)[0];
        if ($.isNumeric(currentTime)){
                     
        } else{
            currentTime = 0
        }
        var section     = $('#row2 a');
		 
        $(section).each(function(i,val){
            var startTime = $(this).attr('data-begin');
            if (startTime <= currentTime) {
               	$(this).prev().removeClass('active')
				$(this).addClass('active');
                findAnnotation();
            } else{
                $(this).removeClass('active')
            }
        });
    }
     
             
    // Find placement of scrubber and show associated Annotation
    var findAnnotation = function(){
		var currTime    = $(audio.currentTime)[0];
        var currTime    = minAndSec(currTime)
        var Annotation      = $('#row2 a');
        var activeAnnotation = $('#row2 a.active')
        var activeAnnTime    = $(activeAnnotation).attr('data-begin');
        var activeAnnTime        = minAndSec(activeAnnTime)
		
		
		
		$("#media div").each(function(i, val){
            var time    = $(this).attr('data-begin');
            var p       = $('p')
			// find active section and mark seen
			$(this).removeClass('active');
			
			
			// add new tab active and fadeIn
			if (activeAnnTime == time){	
				$(this).addClass('active');
				//opacityIn();
				
			} 
			//scroll to the top on tab change
            if (activeAnnTime == currTime){
                $('#media').scrollTop(0);
                 
            }

        });
		
    }
    
	
    $('.mplayer .time-container .completion').html(''); 
    // Get the percentage completed of listened to
	var percentage = function(){
        
        // Check if "completion" is empty 
        if ($('.mplayer .time-container .completion').is(':empty')){
        	
        } else{
        	var max = 812;
        	percent = Math.round(($('.mplayer .track.ui-progressbar .ui-slider-handle').css("left").replace("px","") * 100) / max);
        
        
	        if ($('.mplayer .time-container .completion .percent').html() == "") {
	     		currentPercentage = 0;    //code
	     		
	    	} else {
	     		currentPercentage = parseInt($('.mplayer .time-container .completion .percent').html());
	    	}
        	if (typeof barMoved == "undefined") {
            	barMoved = false;
        	}
        	if ($('.currenttime').html() == "0:00") {
             	barMoved = false;
             	percentageDifference = 0;
	        }
	        //if player is PAUSED
	        if ($('#mediaStart').hasClass('play')) {
	             if (percent > currentPercentage) {
	                  barMoved = true;
	             }
	        }
	    
	        if (barMoved == true) {
				if (percent > currentPercentage) {    
			  		percentageDifference = percent - currentPercentage;    
			 	} else {
			  		percentageDifference = 0
			 	}    
			 	barMoved = false;
			}
			// if player is PLAYING
	        if (barMoved == false) {
				if ((percent > currentPercentage) && ((percent - percentageDifference) > currentPercentage)) {
	                $('.mplayer .time-container .completion .percent').html(percent - percentageDifference);
	            }
	        }
	        // COMPLETED SEND TO API
	        if (currentPercentage == '100'){
				var completed   = "You have listened to <span class='percent bold'>100</span><b>%</b> of this piece.";
				$('.mplayer .time-container .completion').html(completed); 
				
	        }
			if($('.track').hasClass('completed')){
				var completed   = "Your saved score is <span class='percent bold'>100</span><b>%</b>.";
				$('.mplayer .time-container .completion').html(completed); 
			}
			
			
			
			
			
        } // end else if completion is empty
        
        
		
		
		
	}
	
	
             
    // scroll annotations
    var step = 35;
    var scrolling = false;
    var scrolldown       = $('#mediaScroll .page_down');
    var scrollup         = $('#mediaScroll .page_up');
     
     
    var resetScroll = function(){
        $('#media').scrollTop(0);
         
        $(scrollup).addClass('disabled').attr("disabled","disabled");
        $(scrolldown).addClass('disabled').attr("disabled","disabled");
         
        var media = $('#media');
        var active = $('#media div.active');
         
         
        if ($(active).height() > $(media).height()){
 
            $(scrolldown).removeClass('disabled').removeAttr('disabled');
            AnnotationScrolling();
        }
         
    }
    setTimeout(function() {
      resetScroll();
    }, 2000);
	
     
    var AnnotationScrolling = function(){
        var content = $('#media');
        // Wire up events for the 'scrollUp' link:
        $(scrollup).on("click", function (event) {
            // Animates the scrollTop property by the specified step.
            $('#media').animate({
                scrollTop: "-=" + step + "px"
            });
            event.preventDefault();
            if($('#media').scrollTop() == 0){
                $(scrollup).addClass('disabled').attr("disabled","disabled");
            } else {
                 $(scrolldown).removeClass('disabled').removeAttr('disabled');
            }
        });
         
        $(scrolldown).on("click", function (event) {
            $(content).animate({
                scrollTop: "+=" + step + "px"
            });
             event.preventDefault();
             $(scrollup).removeClass('disabled').removeAttr('disabled');
              
             if($('#media').scrollTop() + $('#media').innerHeight() >= $('#media')[0].scrollHeight){
                 $(scrolldown).addClass('disabled').attr("disabled","disabled");
             }
              
        });
    }
    AnnotationScrolling();
	
	
}



 
var fixText = function(){  
     
	 
    /* FIX BRACKET TEXT FROM XML - GLOBAL */
    var mediaText = $('.text');
    
	$(mediaText).each(function(){
        var html        = $(this).text();
        
        html = html.replace(/\n/g, "<br/>").replace(/\[{/g,'<span class="curly">').replace(/\[\|/g,'<span class="mono">').replace(/\[/g,'<span class="brace">').replace(/\]/g,'</span>');
        
        $(this).html(html); 
    });
 
    /* H1 FIX IF TOO LONG ADD CLICK AND TOOLTIP */
    var headline =  $('#player #header h1');
    var title   = headline.text();
    var tooltip = '<div id="headTip">'+ title +'</div>'
     
    $(headline).attr('title', title)
         
    if(title.length >= 80){
        $(headline).text( $(headline).text().trim().substr(0,75) + "..." ).css('cursor', 'pointer');
        $(tooltip).appendTo('#header');
         
        var tooltip = $('#headTip');
        $(headline).click(function(){
            $(tooltip).toggleClass( "highlight" );                
        });
        $(tooltip).click(function(){
            $(headline).click();                     
        })
    }
	
	/* REMOVE "?" IN TITLES FOR IDENTICAL CONTENT */
	var mainSection = $('#mediaTimeline #row1 a');
	$(mainSection).each(function(){
		var title = $(this).attr('title').replace(/\?/g,"");
		var html = $(this).html().replace(/\?/g,"");
		
		$(this).attr('title', title);
		$(this).html(html);
		
	});
	
	var mediaTitle = $('#media div');
	$(mediaTitle).each(function(){
		var html = $(this).html().replace(/\?/g,"");
		$(this).html(html);
		
	});
	
    var annotationDivs = $("#annotation .content div");
	$(annotationDivs).each(function(){
		var id = $(this).attr('id').replace(/\?/,"");
		$(this).attr('id', id);
		
	});
	
	var annotationDiv = $("#annotation .content h3");
	$(annotationDiv).each(function(){
		var html = $(this).html().replace(/\?/g,"");
		$(this).html(html);
		
	});
	
    /* TOOLTIP FOR LIGHT GREEN SECTION */
    var subSection = $('#mediaTimeline #row2 a');
    var title       = $(subSection).attr('title')
             
    $(subSection).each(function(){
        var title   = $(this).attr('title');
        var tooltip = $(this).tooltip({
                        position: {
                            my: "center+10 top-20",
                            at: "center top",
                            collision: "flipfit"
                        },
                        relative: true,
                        content: title,
                        tooltipClass: "mplayerTip"
                      });
                 
         
        
        if (title.length > 17){
			//console.log( title.length);
            var tooltip = $(this).tooltip({
                        position: {
                            my: "center+10 top-20",
                            at: "center top",
                            collision: "flipfit"
                        },
                        relative: true,
                        content: title,
                        tooltipClass: "two"
                      });
        }
                 
    });
     
     
    /* FIND LAST SUBSECTION */
    var subSection = $('#mediaTimeline #row2 a');
    var obj = {};
    var num = 0;
    $(subSection).each(function(){
        var className   = $(this).attr('class');
 
        //loop through classes find last one and add class "last"
        if (!obj[className]){
            obj[className] = {};
            num++
            $(this).last().addClass('last').css({'border-left': '4px solid #fff'});
             
            var lastWidth   = $(this).prev().width();
            var newWidth    = lastWidth - '3'
             
            $(this).prev().width(newWidth);
             
        }
        $(subSection).first().removeClass('last').css({'border-left': '1px solid #fff'});
             
    });
     
     
} // end fixText();

