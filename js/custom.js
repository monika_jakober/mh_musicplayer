

$(function() {
		$( "#tabs" ).tabs({
			active: 2
		});
		setTimeout(function() {
    		$('.playerTip').fadeOut('slow');
		}, 8000);
		
		
		var url 	= window.location.pathname;
		var name 	= url.substring(url.lastIndexOf('/') + 1).split(".")[0];
		var xmlPath 	= 'xml/'+name+'.xml';
		
		$.ajax({
			url: xmlPath,
			dataType: 'xml',
			success: parseXml,
			error: function(data){
				console.log('Error loading XML data');
			}
		});
		
		function minAndSec(sec) {
			sec = parseInt(sec);
			return Math.floor(sec / 60) + ":" + (sec % 60 < 10 ? '0' : '') + Math.floor(sec % 60);
		}
		
		function parseXml(xml){
			$(xml).find("main").each(function(){
				var title 		= $(this).attr("guideTitle");
				var subTitle	= $(this).attr("guideSubtitle");
				var heading		= $(this).attr("listeningHeading");
				var mp3			= $(this).attr("mp3");
				
				var duration	= $(this).attr("mp3duration");
				var dur			= minAndSec(duration);
				var widthLeft 	= $("#mediaTimeline").css("width").replace("px","")-1;
				//console.log(duration)
				
				// ANNOTATION AREA
				var media 		= $("#tabs #player #media");

				// PLAYER AREA
				var mediaTimeline1 = $("#topbar #mediaController #mediaTimeline #row1");
				var mediaTimeline2 = $("#topbar #mediaController #mediaTimeline #row2")

				var background 	= $(this).find("item[title='Background Info']").children("item[itemtype='paragraph']").attr('content');
				var guide 		= $(this).find("item[title='Listening Guide']");

				
				$("#tabs #player .highlight #header h1, #tabs #background h3, #tabs #annotation h3").append(title + " " + subTitle);
				$(".top.mediapadding").append('<div class="heading">'+heading+'</div>');
				
				$("#tabs #background .content").append('<p class="text">' + background +'</p>')
				$(media).attr("data-dur",dur);
				$(media).append('<div class="duration">'+duration+'</div>');
				
				
				// SET AUDIO EXTENSION FOR BROWSER
				var audio	= mp3.split('.')[0];
				
				if('MozBoxSizing' in document.body.style){	
					$("#tabs #player #audioPlayer").attr("src", "mp3/"+audio+".ogg");
					
				} else{
					$("#tabs #player #audioPlayer").attr("src", "mp3/"+audio+".mp3");
				}
				
				 

				$(guide).each(function(){
					var sectionCount 	= 1;
					var section 		= $(this).find("item[itemtype='section']");
					//console.log(section.length);
					

					var sectionStart = [];
					var sectionLengths = [];
					var subsectionStart = [];
					var subsectionLengths = [];
					var sectionIndex = 0;
					var subsectionTotal = 0;
					var pixelPerSecondSection = ($("#mediaTimeline").css("width").replace("px","") - $(section).length)/duration;

					$(section).each(function(){
                    	var sectionTitle	= $(this).attr('title');
						var sectionTime		= $(this).attr('time');
						
						sectionStart[sectionIndex] = [sectionTitle, parseInt(sectionTime)];
						sectionIndex++;
						  
						var subSection = $(this).find("item[itemtype='subsection']");
							subsectionIndex = 0;
						  	subsectionStart[sectionTitle] = [];
						
						$(subSection).each(function(){
							subsectionStart[sectionTitle].push([subsectionIndex,parseInt($(this).attr('time'))]);
						  	subsectionIndex++;
						  	subsectionTotal++;
						}); //end of subsection
					}); // end of section

					fullLength = 0;
					
					sectionStart.forEach(function(elem,index){
						if (index < sectionStart.length - 1) {
							thisLength = sectionStart[index+1][1] - elem[1];
						} else {
							thisLength = duration - elem[1];
						}
						
						sectionLengths[elem[0]] = thisLength;
						fullLength += thisLength;
						sectionTitle = elem[0];
						subsectionLengths[sectionTitle] = [];
						subsectionStart[sectionTitle].forEach(function(elem,index){
							if (index < subsectionStart[sectionTitle].length - 1) {
								thisSubLength = subsectionStart[sectionTitle][index+1][1] - elem[1];
							} else {
								thisSubLength = fullLength - elem[1] ;
							}
							subsectionLengths[sectionTitle].push([index,thisSubLength]);
						});
					});

					var pixelPerSecondSubSection = ($("#mediaTimeline").css("width").replace("px","") - (subsectionTotal))/duration;
					
					$(section).each(function(){
						var sectionTitle	= $(this).attr('title');
						var sectionTime		= $(this).attr('time');
						var sectionDuration     = sectionLengths[sectionTitle];
						var sectionWidth        = Math.round(sectionDuration * pixelPerSecondSection);
						
						if (sectionWidth > widthLeft) {
							sectionWidth = widthLeft - 1;
						}
						widthLeft = widthLeft - sectionWidth -1;
						//console.log(pixelPerSecondSubSection)
						
						// POPULATE DARK GREEN SECTION (MAIN SECTIONS)
						$(mediaTimeline1).append('<a title="'+sectionTitle +'" data-begin="'+sectionTime+'" style="width: ' + sectionWidth +  'px;">'+ sectionTitle +'</a>');

						// POPULATE LIGHT GREEN SECTION (SUBSECTIONS)
						var subSection 		= $(this).find("item[itemtype='subsection']");
						var subParentTitle	= $(subSection).parent().attr('title');
						var subSecTitle		= $(subSection).attr('title');
						var subSecTime		= $(subSection).attr('time');
						var countSubsections= $(subSection).length;
						//console.log(countSubsections);


						// IN ONE SECTION ADD SUBSECTION TIMES TO DETERMIN WIDTH OF DARK GREEN SECTION

						var subsectionIndex = 0;
						var totalSubsectionWidth = 0;
						
						// ANNOTATION SET UP FOR ANNOTATION TAB
						var annotation = '<div id="'+subParentTitle +'"><h3>' + subParentTitle+'</h3>';
						
						$(subSection).each(function(){
							subSecTitle				= $(this).attr('title');
							subsectionTime			= $(this).attr('time');
							
							subTime					= minAndSec(subsectionTime);
							subsectionWidth     	= Math.round(subsectionLengths[sectionTitle][subsectionIndex][1] * pixelPerSecondSubSection);
							totalSubsectionWidth    += subsectionWidth + 1;
							
							if (subsectionIndex == $(subSection).length -1) {
								//console.log(totalSubsectionWidth);
							     if (totalSubsectionWidth - 1  < sectionWidth) {
								 	subsectionWidth = subsectionWidth + (sectionWidth - (totalSubsectionWidth -1));
								 } else if (totalSubsectionWidth - 1 > sectionWidth){
								  	subsectionWidth = subsectionWidth - ((totalSubsectionWidth -1) - sectionWidth);
								 }

							}
							
							// POPULATE LIGHT GREEN SECTION (SUB SECTIONS)
							$(mediaTimeline2).append('<a class="'+ sectionTitle +'" title="'+ subSecTitle +' (' +subTime +')" data-begin="'+subsectionTime+'" style="width:' + subsectionWidth + 'px;"><span>'+ sectionTitle + ':' + subSecTitle +' - ' +subsectionTime +'</span></a>');
							
							// ANNOTATION SET UP FOR PLAYER
							var output = '<div id="'+sectionTitle +'" class="section" data-begin="'+subTime+'" rel="'+ subTime +'"><b>'+ sectionTitle +'</b> <br/>';
  							
							annotation += '<span class="subTime">'+ subTime +'</span>'
							
							// FIND EACH SUBSECTION ITEM
							$(this).find('item').each(function () {
								var time	= $(this).attr('time');
     							var content = $(this).attr('content');
								var itemtype = $(this).attr('itemtype');
								var language = $(this).attr('language');
								//content.html().find('&#xD;').replaceWith('<br/>')
								
								
								
								if (itemtype == 'image'){
									output += '<p class="img"><img src="images/' + content +'"/></p>'
									annotation += '<p class="img"><img src="images/' + content +'"/></p>'
								} 
								if (itemtype == 'paragraph' && language == ''){
									output += '<p class="text">' + content +'</p>';
									annotation += '<p class="text" data-begin="'+subTime+'">' + content + ' </p>';
								}
								if (itemtype == 'paragraph' && language == 'Non-English'){
									output += '<p class="text nonEnglish">' + content +'</p>';
									annotation += '<p class="text nonEnglish">' + content + ' </p>';
								}
								if (itemtype == 'paragraph' && language == 'English'){
									output += '<p class="text english">' + content +'</p>';
									annotation += '<p class="text english">' + content + ' </p>';
								}
								
    						});
							
							output += '</div>';
							
							$('#media').append(output);
							
							subsectionIndex++;
						
						}); // end of subsection
						
						annotation += '</div>';
						$('#annotation .content').append(annotation);
						
					});
				});
  			}); // end main each function
			makeAudio();
			fixText();
			
		}
		
		
		
		
});

	
