<component name="Accordion Tree Menu v3">
  <data>
    <main childStyle="level1title_style" guideTitle="DEBUSSY, [Prélude à l’Après-midi d’un faune]" guideSubtitle="[Prelude to the Afternoon of a Faun]; 1894" listeningHeading="At a very moderate tempo, A B A form, E major" mp3="debussy_prelude_a_lapresmidi_d_un_faune.mp3" mp3duration="583" showoutline="all">
      <item title="General Info" childStyle="level1_style" function="setGeneral(true)"/>
      <item title="Background Info" childStyle="level1_style" function="setBackground(true)">
        <item title="Text: The music" itemtype="paragraph" content="“The music of this Prelude,” wrote Debussy of his [Prelude to the Afternoon of a Faun], “is a very free illustration of the beautiful poem by Stéphane Mallarmé, [The Afternoon of a Faun].” This poem evokes the dreams and erotic fantasies of a pagan forest creature who is half man, half goat. While playing a “long solo” on his flute, the intoxicated faun tries to recall whether he actually carried off two beautiful nymphs or only dreamed of doing so. Exhausted by the effort, he falls back to sleep in the sunshine. &#xD;&#xD;Debussy intended his music to suggest “the successive scenes through which pass the desires and dreams of the faun in the heat of this afternoon.” The subtle, sensuous timbres of this miniature tone poem were new in Debussy’s day. Woodwind solos, muted horn calls, and harp glissandos create a rich variety of delicate sounds. The dynamics are usually subdued, and only rarely does the entire orchestra—from which trombones, trumpets, and timpani are excluded—play at one time. The music often swells sensuously and then subsides in voluptuous exhaustion. &#xD;&#xD;The prelude begins with an unaccompanied flute melody; its vague pulse and tonality make it dreamlike and improvisatory. This flute melody is heard again and again, faster, slower, and against a variety of lush chords. Though the form of the prelude may be thought of as A B A', one section blends with the next. It has a continuous ebb and flow. The fluidity and weightlessness typical of impressionism are found in this music. We are never tempted to beat time to its subtle rhythms. The prelude ends magically with the main melody, played by muted horns, seeming to come from far off. The bell-like tones of antique cymbals finally evaporate into silence. With all its new sounds and musical techniques, the piece has aptly been described as a “quiet revolution” in the history of music.&#xD;&#xD;_________________________________________________________________&#xD;[Debussy: Prélude a l'après-midi d'un faune&#xD;Philharmonia Orchestra; Pierre Boulez, conductor&#xD;Originally released 1969. All rights reserved by Sony Music Entertainment]" time="0" language=""/>
      </item>
      <item title="Listening Guide" childStyle="level1_style" heading="" function="setListening(true)">
        <item title="A Section" childStyle="level2_style" time="0" itemtype="section">
          <item title="1.a." childStyle="level3_style" time="0" itemtype="subsection">
            <item title="Text: 1. a...." itemtype="paragraph" content="1. a. Solo flute, [p], main melody." time="0" language=""/>
            <item title="Image: 520 x 130" itemtype="image" content="kam25087_070402a.png" w="520" h="130" time="0" duration=""/>
            <item title="Text: Harp glissando;" itemtype="paragraph" content="Harp glissando; soft horn calls. Short pause. Harp glissando; soft horn calls." time="0" language=""/>
          </item>
          <item title="1.b." childStyle="level3_style" time="43.9" itemtype="subsection">
            <item title="Text: 1. b...." itemtype="paragraph" content="1. b.  Flute, [p], main melody; tremolo strings in background. Oboe, [p], continues melody.  Orchestra swells to [f]. Solo clarinet fades into" time="43" language=""/>
          </item>
          <item title="1.c." childStyle="level3_style" time="95" itemtype="subsection">
            <item title="Text: 1. c...." itemtype="paragraph" content="1. c.  Flute, [p], main melody varied and expanded; harp and muted strings accompany.   Flute  melody comes to quiet close." time="95" language=""/>
          </item>
          <item title="2.a." childStyle="level3_style" time="168" itemtype="subsection">
            <item title="Text: 2. a...." itemtype="paragraph" content="2. a.  Clarinet; harp and cellos in background." time="168" language=""/>
          </item>
          <item title="2.b." childStyle="level3_style" time="196.2" itemtype="subsection">
            <item title="Text: 2. b...." itemtype="paragraph" content="2. b.  New oboe melody." time="196.2" language=""/>
            <item title="Image: 520 x 53" itemtype="image" content="kam25087_070402b.png" w="520" h="53" time="196" duration=""/>
            <item title="Text: Violins take..." itemtype="paragraph" content="Violins take up melody, crescendo and accelerando to climax. Excitement subsides.  Ritardando. Clarinet, [p], leads into" time="196" language=""/>
          </item>
        </item>
        <item title="B Section" childStyle="level2_style" time="273" itemtype="section">
          <item title="3.a." childStyle="level3_style" time="273.5" itemtype="subsection">
            <item title="Text: 3. a...." itemtype="paragraph" content="3. a.  Woodwinds, [p], legato melody in long notes. Crescendo." time="273.5" language=""/>
            <item title="Image: 520 x 75" itemtype="image" content="kam25087_070402c.png" w="520" h="75" time="273" duration=""/>
          </item>
          <item title="3.b." childStyle="level3_style" time="316" itemtype="subsection">
            <item title="Text: 3. b...." itemtype="paragraph" content="3. b.  Strings repeat melody; harps and pulsating woodwinds in background, crescendo.  Decrescendo. Horns, [p], solo violin, [p], clarinet, oboe. " time="316" language=""/>
          </item>
        </item>
        <item title="A' Section" childStyle="level2_style" time="381" itemtype="section">
          <item title="4.a." childStyle="level3_style" time="381" itemtype="subsection">
            <item title="Text: 4. a...." itemtype="paragraph" content="4. a.  Harp accompanies flute, [p], main melody in longer notes. Oboe, staccato woodwinds." time="381" language=""/>
          </item>
          <item title="4.b." childStyle="level3_style" time="416.75" itemtype="subsection">
            <item title="Text: 4. b...." itemtype="paragraph" content="4. b.   Harp accompanies oboe, [p], main melody in longer notes. English horn, harp glissando." time="416" language=""/>
          </item>
          <item title="5.a." childStyle="level3_style" time="459" itemtype="subsection">
            <item title="Text: 5. a...." itemtype="paragraph" content="5. a.  Antique cymbals, bell-like tones. Flutes, [p], main melody. Solo violins, [pp], in high register. " time="459" language=""/>
          </item>
          <item title="5.b." childStyle="level3_style" time="496.75" itemtype="subsection">
            <item title="Text: 5. b...." itemtype="paragraph" content="5. b.  Flute and solo cello, main melody; harp in background." time="496" language=""/>
          </item>
          <item title="5.c." childStyle="level3_style" time="525" itemtype="subsection">
            <item title="Text: 5. c...." itemtype="paragraph" content="5. c.  Oboe, [p], brings melody to close. Harps, [p]." time="525" language=""/>
          </item>
          <item title="5.d." childStyle="level3_style" time="550.5" itemtype="subsection">
            <item title="Text: 5. d...." itemtype="paragraph" content="5. d.   Muted horns and violins, [ppp], beginning of main melody sounding far off. Flute, antique cymbals, and harp; delicate tones fade into silence." time="550.5" language=""/>
          </item>
        </item>
      </item>
      <item title="Final Steps" childStyle="level1_style" function="setFinal(true)"/>
    </main>
  </data>
  <styles>
    <level1title_style>
      <folderCollapsedIconId>ATM_default_style1_collapsed_icon</folderCollapsedIconId>
      <folderExpandedIconId>ATM_default_style1_expanded_icon</folderExpandedIconId>
      <spacing>1</spacing>
      <itemHeight>24</itemHeight>
      <itemNextSpacing>1</itemNextSpacing>
      <textFormat>
        <font>Arial</font>
        <size>16</size>
        <color>0xffffff</color>
        <align>left</align>
        <leftMargin>0</leftMargin>
        <rightMargin>4</rightMargin>
      </textFormat>
      <states>
        <up>
          <mcLinkageId>ATM_default_style1_false_up</mcLinkageId>
        </up>
        <over>
          <mcLinkageId>ATM_default_style1_false_over</mcLinkageId>
          <textFormat>
            <color>0x333333</color>
          </textFormat>
        </over>
        <selected>
          <mcLinkageId>ATM_default_style1_false_selected</mcLinkageId>
        </selected>
      </states>
    </level1title_style>
    <level1_style>
      <folderCollapsedIconId>ATM_default_style1_collapsed_icon</folderCollapsedIconId>
      <folderExpandedIconId>ATM_default_style1_expanded_icon</folderExpandedIconId>
      <spacing>1</spacing>
      <itemHeight>24</itemHeight>
      <itemNextSpacing>1</itemNextSpacing>
      <textFormat>
        <font>Arial</font>
        <size>15</size>
        <color>0xffffff</color>
        <align>left</align>
        <leftMargin>0</leftMargin>
        <rightMargin>4</rightMargin>
      </textFormat>
      <states>
        <up>
          <mcLinkageId>ATM_default_style1_false_up</mcLinkageId>
        </up>
        <over>
          <mcLinkageId>ATM_default_style1_false_over</mcLinkageId>
          <textFormat>
            <color>0x333333</color>
          </textFormat>
        </over>
        <selected>
          <mcLinkageId>ATM_default_style1_false_selected</mcLinkageId>
        </selected>
      </states>
    </level1_style>
    <level2_style>
      <folderCollapsedIconId>ATM_default_style1_collapsed_icon</folderCollapsedIconId>
      <folderExpandedIconId>ATM_default_style1_expanded_icon</folderExpandedIconId>
      <spacing>1</spacing>
      <itemHeight>22</itemHeight>
      <itemNextSpacing>1</itemNextSpacing>
      <textFormat>
        <font>Arial</font>
        <size>14</size>
        <color>0xffffff</color>
      </textFormat>
      <states>
        <up>
          <mcLinkageId>ATM_default_style2_false_up</mcLinkageId>
        </up>
        <over>
          <mcLinkageId>ATM_default_style2_false_over</mcLinkageId>
          <textFormat>
            <color>0x333333</color>
          </textFormat>
        </over>
        <selected>
          <mcLinkageId>ATM_default_style2_false_selected</mcLinkageId>
        </selected>
      </states>
    </level2_style>
    <level3_style>
      <folderCollapsedIconId>ATM_default_style1_collapsed_icon</folderCollapsedIconId>
      <folderExpandedIconId>ATM_default_style1_expanded_icon</folderExpandedIconId>
      <spacing>1</spacing>
      <itemHeight>20</itemHeight>
      <itemNextSpacing>1</itemNextSpacing>
      <textFormat>
        <font>Arial</font>
        <size>13</size>
        <color>0xffffff</color>
      </textFormat>
      <states>
        <up>
          <mcLinkageId>ATM_default_style3_false_up</mcLinkageId>
        </up>
        <over>
          <mcLinkageId>ATM_default_style3_false_over</mcLinkageId>
          <textFormat>
            <color>0x333333</color>
          </textFormat>
        </over>
        <selected>
          <mcLinkageId>ATM_default_style3_false_selected</mcLinkageId>
        </selected>
      </states>
    </level3_style>
    <level4_style>
      <folderCollapsedIconId>ATM_default_style1_collapsed_icon</folderCollapsedIconId>
      <folderExpandedIconId>ATM_default_style1_expanded_icon</folderExpandedIconId>
      <spacing>1</spacing>
      <itemHeight>22</itemHeight>
      <itemNextSpacing>1</itemNextSpacing>
      <textFormat>
        <font>Arial</font>
        <size>12</size>
        <color>0xffffff</color>
      </textFormat>
      <states>
        <up>
          <mcLinkageId>ATM_default_style2_false_up</mcLinkageId>
        </up>
        <over>
          <mcLinkageId>ATM_default_style2_false_over</mcLinkageId>
          <textFormat>
            <color>0x333333</color>
          </textFormat>
        </over>
        <selected>
          <mcLinkageId>ATM_default_style2_false_selected</mcLinkageId>
        </selected>
      </states>
    </level4_style>
  </styles>
</component>