<component name="Accordion Tree Menu v3">
  <data>
    <main childStyle="level1title_style" guideTitle="MAHLER, [Ging heut’ Morgen über’s Feld] ([This Morning I Went through the Fields])" guideSubtitle="[Lieder eines fahrenden Gesellen] ([Songs of a Wayfarer])" listeningHeading="Modified strophic form." mp3="mahler_ging_heut_morgen.mp3" mp3duration="259.23" showoutline="all">
      <item title="General Info" childStyle="level1_style" function="setGeneral(true)"/>
      <item title="Background Info" childStyle="level1_style" function="setBackground(true)">
        <item title="Text: Composed 18831" itemtype="paragraph" content="[{Composed 1883–1885, Orchestrated 1891–1896] &#xD;Mahler’s first masterpiece was [Lieder eines fahrenden Gesellen] ([Songs of a Wayfarer]), a cycle of four songs for voice and orchestra. He wrote both the words and the music of this autobiographical song cycle at the age of twenty-four, under the impact of an unhappy love affair with Johanna Richter, a soprano in the opera company where he was a conductor. “What can these songs tell her that she does not know already?” Mahler confided to a friend. “The cycle deals with a young wayfarer ill-treated by destiny, who sets out in the wide world and wanders about aimlessly.”&#xD;&#xD;Mahler’s depiction of an alienated wanderer, a frequent theme in German romantic poetry, was probably influenced by Schubert’s song cycle [Winterreise] ([Winter’s Journey]; 1827). To convey a feeling of restlessness, Mahler begins and ends each song of his cycle in a different key. He would later use melodies from the second and fourth of the  [Songs of a Wayfarer] as themes in his next work, his First Symphony (1885–1888). We’ll focus on the second song of the cycle.&#xD;&#xD;[{Ging heut’ Morgen über’s Feld&#xD;(This Morning I Went through the Fields) ]&#xD;[Ging heut’ Morgen über’s Feld] reflects Mahler’s attraction to nature and to folklike simplicity. The song is in modified strophic form, with brief orchestral interludes connecting the different stanzas. In the first three stanzas of the poem the wayfarer tells how he walks through the fields on a beautiful spring morning and is greeted merrily by the flowers and birds. The final stanza brings a darkening of mood with the wanderer’s realization that happiness is beyond his reach.&#xD;&#xD;The first two stanzas are sung to the same lighthearted melody (A) in folk style. The last two stanzas begin with varied forms of the opening phrase of A in a new major key, but then continue with different music. The sadness of the last stanza is conveyed by a slow tempo, which contrasts with the moderate tempo of the carefree earlier stanzas. Mahler expresses the pain of the wayfarer’s final realization—“No! No! What I love can never bloom for me!”—by a whispered high melody with poignant dissonances on the words [nimmer] ([never]) and [blühen] ([bloom]). An orchestral postlude featuring harp and solo violin rounds off the song on a note of unfulfilled longing." time="0" language=""/>
      </item>
      <item title="Listening Guide" childStyle="level1_style" heading="" function="setListening(true)">
        <item title="Song" childStyle="level2_style" time="0" itemtype="section">
          <item title="A" childStyle="level3_style" time="0" itemtype="subsection">
            <item title="Text: At a" itemtype="paragraph" content="[{At a comfortable pace; flutes, repeated notes, introduce]&#xD;&#xD;[{A]" time="0" language=""/>
            <item title="Text: Ging heut" itemtype="paragraph" content="[Ging heut’ Morgen über’s Feld,]" time="0" language="Non-English"/>
            <item title="Text: I went" itemtype="paragraph" content="I went out this morning over the countryside;" time="0" language="English"/>
            <item title="Image: 520 x 60" itemtype="image" content="kam25087_062001a.png" w="520" h="60" time="0" duration=""/>
            <item title="Text: Tau noch" itemtype="paragraph" content="[Tau noch auf den Gräsern hing,&#xD;sprach zu mir der lust’ge Fink:&#xD;“Ei, du! Gelt?&#xD;Guten Morgen!&#xD;Wird’s nicht eine schöne Welt?&#xD;Zink! Zink! Schön und fink!&#xD;Wie mir doch die Welt gefällt!”]" time="0" language="Non-English"/>
            <item title="Text: dew still" itemtype="paragraph" content="dew still hung from the grass; &#xD;the merry finch spoke to me:&#xD;“Oh, it’s you, is it!&#xD;Good morning! &#xD;Is it not a lovely world? &#xD;Chirp! Chirp! Pretty and lively! &#xD;How the world delights me!”" time="0" language="English"/>
            <item title="Text: String interlud" itemtype="paragraph" content="[{String interlude, crescendo, trumpet.]" time="0" language=""/>
          </item>
          <item title="A" childStyle="level3_style" time="41" itemtype="subsection">
            <item title="Text: Auch die" itemtype="paragraph" content="[Auch die Glockenblum’ am Feld]" time="41" language="Non-English"/>
            <item title="Text: The bluebells" itemtype="paragraph" content="The bluebells in the meadow also" time="41" language="English"/>
            <item title="Text: Cellos p" itemtype="paragraph" content="[{Cellos, ][p][{, imitate voice. Glockenspiel]" time="41" language=""/>
            <item title="Text: hat mir" itemtype="paragraph" content="[hat mir lustig, guter Ding’, &#xD;mit den Glöckchen, &#xD;klinge, kling, &#xD;ihren Morgengruss geschellt: &#xD;“Wird’s nicht eine schöne Welt!? &#xD;Kling, kling! Schönes Ding! &#xD;Wie mir doch die Welt gefällt! &#xD;Heia!&quot;]" time="41" language="Non-English"/>
            <item title="Text: rang merrily" itemtype="paragraph" content="rang merrily and cheerfully for &#xD;me with their little bells, &#xD;ring-a-ring, &#xD;rang their morning greeting: &#xD;“Is it not a lovely world!? &#xD;Ring, ring! Pretty thing! &#xD;How the world delights me! &#xD;Ho!”" time="41" language="English"/>
            <item title="Text: String interlud" itemtype="paragraph" content="[{String interlude, clarinets and harp introduce]" time="41" language=""/>
          </item>
          <item title="A opening (new key)" childStyle="level3_style" time="89" itemtype="subsection">
            <item title="Text: Opening of" itemtype="paragraph" content="[{Opening of A in new key, with high sustained tone in voice.]" time="89" language=""/>
            <item title="Text: Und da" itemtype="paragraph" content="[Und da fing im Sonnenschein]" time="89" language="Non-English"/>
            <item title="Text: And then" itemtype="paragraph" content="And then in the sunshine" time="89" language="English"/>
            <item title="Image: 520 x 89" itemtype="image" content="kam25087_062001b.png" w="520" h="89" time="89" duration=""/>
            <item title="Text: gleich die" itemtype="paragraph" content="[gleich die Welt zu &#xD;funkeln an; Alles, alles &#xD;Ton und Farbe gewann im &#xD;Sonnenschein!&#xD;Blum’ und Vogel, &#xD;Gross und Klein! &#xD;“Guten Tag, guten Tag!&#xD;Ist’s nicht eine schöne Welt?&#xD;Ei, du Gelt? &#xD;Schöne Welt!?”]" time="89" language="Non-English"/>
            <item title="Text: the world" itemtype="paragraph" content="the world at once began to &#xD;sparkle, everything, everything &#xD;took on sound and color in the &#xD;sunshine! &#xD;Flower and bird, &#xD;the large and the small! &#xD;“Good day! good day! &#xD;Isn’t it a lovely world? &#xD;Don’t you agree? &#xD;Lovely world!” " time="89" language="English"/>
            <item title="Text: Tempo slows" itemtype="paragraph" content="[{Tempo slows, repeated notes in cellos introduce]" time="143.2" language=""/>
          </item>
          <item title="A opening (slower)" childStyle="level3_style" time="154" itemtype="subsection">
            <item title="Text: Opening of" itemtype="paragraph" content="[{Opening of A in voice, slow tempo, imitated by oboe and muted cellos. Flutes, clarinets, timpani,] [pp][{.]&#xD;" time="154" language=""/>
            <item title="Text: Nun fngt" itemtype="paragraph" content="[Nun fängt auch mein Glück wohl an?!&#xD;Nun fängt auch mein Glück wohl an?!]" time="154" language="Non-English"/>
            <item title="Text: Now surely" itemtype="paragraph" content="Now surely my happiness also begins?!&#xD;Now surely my happiness also begins?!" time="154" language="English"/>
            <item title="Image: 469 x 78" itemtype="image" content="kam25087_062002.png" w="469" h="78" time="154" duration=""/>
            <item title="Text: Nein Nein" itemtype="paragraph" content="[Nein! Nein! Das ich mein’,&#xD;mir nimmer nimmer blühen kann!]" time="154" language="Non-English"/>
            <item title="Text: No No" itemtype="paragraph" content="No! No! What I love &#xD;can never, never bloom for me!" time="154" language="English"/>
          </item>
        </item>
        <item title="Postludeextralong title here" childStyle="level2_style" time="233" itemtype="section">
          <item title=" " childStyle="level3_style" time="233" itemtype="subsection">
            <item title="Text: Postlude harp" itemtype="paragraph" content="[{Postlude: harp, muted solo violin, ][ppp][{ ending.]" time="233" language=""/>
          </item>
        </item>
      </item>
      <item title="Final Steps" childStyle="level1_style" function="setFinal(true)"/>
    </main>
  </data>
  <styles>
    <level1title_style>
      <folderCollapsedIconId>ATM_default_style1_collapsed_icon</folderCollapsedIconId>
      <folderExpandedIconId>ATM_default_style1_expanded_icon</folderExpandedIconId>
      <spacing>1</spacing>
      <itemHeight>24</itemHeight>
      <itemNextSpacing>1</itemNextSpacing>
      <textFormat>
        <font>Arial</font>
        <size>16</size>
        <color>0xffffff</color>
        <align>left</align>
        <leftMargin>0</leftMargin>
        <rightMargin>4</rightMargin>
      </textFormat>
      <states>
        <up>
          <mcLinkageId>ATM_default_style1_false_up</mcLinkageId>
        </up>
        <over>
          <mcLinkageId>ATM_default_style1_false_over</mcLinkageId>
          <textFormat>
            <color>0x333333</color>
          </textFormat>
        </over>
        <selected>
          <mcLinkageId>ATM_default_style1_false_selected</mcLinkageId>
        </selected>
      </states>
    </level1title_style>
    <level1_style>
      <folderCollapsedIconId>ATM_default_style1_collapsed_icon</folderCollapsedIconId>
      <folderExpandedIconId>ATM_default_style1_expanded_icon</folderExpandedIconId>
      <spacing>1</spacing>
      <itemHeight>24</itemHeight>
      <itemNextSpacing>1</itemNextSpacing>
      <textFormat>
        <font>Arial</font>
        <size>15</size>
        <color>0xffffff</color>
        <align>left</align>
        <leftMargin>0</leftMargin>
        <rightMargin>4</rightMargin>
      </textFormat>
      <states>
        <up>
          <mcLinkageId>ATM_default_style1_false_up</mcLinkageId>
        </up>
        <over>
          <mcLinkageId>ATM_default_style1_false_over</mcLinkageId>
          <textFormat>
            <color>0x333333</color>
          </textFormat>
        </over>
        <selected>
          <mcLinkageId>ATM_default_style1_false_selected</mcLinkageId>
        </selected>
      </states>
    </level1_style>
    <level2_style>
      <folderCollapsedIconId>ATM_default_style1_collapsed_icon</folderCollapsedIconId>
      <folderExpandedIconId>ATM_default_style1_expanded_icon</folderExpandedIconId>
      <spacing>1</spacing>
      <itemHeight>22</itemHeight>
      <itemNextSpacing>1</itemNextSpacing>
      <textFormat>
        <font>Arial</font>
        <size>14</size>
        <color>0xffffff</color>
      </textFormat>
      <states>
        <up>
          <mcLinkageId>ATM_default_style2_false_up</mcLinkageId>
        </up>
        <over>
          <mcLinkageId>ATM_default_style2_false_over</mcLinkageId>
          <textFormat>
            <color>0x333333</color>
          </textFormat>
        </over>
        <selected>
          <mcLinkageId>ATM_default_style2_false_selected</mcLinkageId>
        </selected>
      </states>
    </level2_style>
    <level3_style>
      <folderCollapsedIconId>ATM_default_style1_collapsed_icon</folderCollapsedIconId>
      <folderExpandedIconId>ATM_default_style1_expanded_icon</folderExpandedIconId>
      <spacing>1</spacing>
      <itemHeight>20</itemHeight>
      <itemNextSpacing>1</itemNextSpacing>
      <textFormat>
        <font>Arial</font>
        <size>13</size>
        <color>0xffffff</color>
      </textFormat>
      <states>
        <up>
          <mcLinkageId>ATM_default_style3_false_up</mcLinkageId>
        </up>
        <over>
          <mcLinkageId>ATM_default_style3_false_over</mcLinkageId>
          <textFormat>
            <color>0x333333</color>
          </textFormat>
        </over>
        <selected>
          <mcLinkageId>ATM_default_style3_false_selected</mcLinkageId>
        </selected>
      </states>
    </level3_style>
    <level4_style>
      <folderCollapsedIconId>ATM_default_style1_collapsed_icon</folderCollapsedIconId>
      <folderExpandedIconId>ATM_default_style1_expanded_icon</folderExpandedIconId>
      <spacing>1</spacing>
      <itemHeight>22</itemHeight>
      <itemNextSpacing>1</itemNextSpacing>
      <textFormat>
        <font>Arial</font>
        <size>12</size>
        <color>0xffffff</color>
      </textFormat>
      <states>
        <up>
          <mcLinkageId>ATM_default_style2_false_up</mcLinkageId>
        </up>
        <over>
          <mcLinkageId>ATM_default_style2_false_over</mcLinkageId>
          <textFormat>
            <color>0x333333</color>
          </textFormat>
        </over>
        <selected>
          <mcLinkageId>ATM_default_style2_false_selected</mcLinkageId>
        </selected>
      </states>
    </level4_style>
  </styles>
</component>